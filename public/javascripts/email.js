var emailStyle = `
#email{
  position: absolute;
  top: 0;
  left: 0;
  font: 15px "Lucida Grande", Helvetica, Arial, sans-serif;
  background-color: rgb(74, 79, 84) !important;
  width: 97.5vw;
  height: 100vh;
  color: white;
  padding: 20px;
  overflow: hidden;
  text-align: center;
}
#link{
  word-break: break-all;
}
a, a:hover, a:active, a:visited { 
  color: white; 
}
#content{
  width: 40vw;
  margin: auto
}
.btn {
display: inline-block;
font-weight: 400;
text-align: center;
white-space: nowrap;
vertical-align: middle;
-webkit-user-select: none;
-moz-user-select: none;
-ms-user-select: none;
user-select: none;
border: 1px solid transparent;
padding: 0.375rem 0.75rem;
font-size: 1rem;
line-height: 1.5;
border-radius: 0.25rem;
transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
}


.btn-info {
color: #fff;
background-color: #17a2b8;
border-color: #17a2b8;
}

.btn-info:hover {
color: #fff;
background-color: #138496;
border-color: #117a8b;
}

.btn-info:focus, .btn-info.focus {
box-shadow: 0 0 0 0.2rem rgba(23, 162, 184, 0.5);
}
`

module.exports = emailStyle;


