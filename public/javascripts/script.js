function showFileName () {
    var name = document.getElementById('postpicture'); 
    document.getElementById('filename').innerHTML = '<i class="fas fa-images"></i> ' + name.files.item(0).name;
  };

$(document).ready(function() {
  function checkIfUserVoted(item) {
      return true ? localStorage.getItem(item) === "true" : false
  }
    
  $(".btnPostlike").click(function(e){
    let postid = $(this).attr("data-id");

    e.preventDefault();
    if (!checkIfUserVoted(postid)) {
      $(this).attr("disabled", true);
      localStorage.setItem(postid, "true");
      $.ajax({
        type: 'POST',
        url: '/posts/postlike',
        data: { postid: $(this).attr("data-id") },
        dataType: 'json',
        });
        var likes = parseInt($(this).children('span').html());
        $(this).children('span').html(likes+1);
        $(this).children('span').css("display","block");
        $(this).children('i').css("display","none");
    }  
  });
});

function showVoted(){
  $( ".btnPostlike" ).each(function( index ) {
    if(localStorage.getItem($( this ).attr("data-id")) === "true"){
      $(this).attr("disabled", true);
      $(this).children('span').css("display","block");
      $(this).children('i').css("display","none");
    }
  });

  $('li.active').removeClass('active');
  $('a[href="https://projektweb20.herokuapp.com' + location.pathname + '"]').closest('li').addClass('active'); 
}


