var express = require('express');
var router = express.Router();
var Comment = require('../models/comment.js');
var checkAuthentication = require('../utils/checkAuthentication');

//dodanie komentarza
router.post('/addcomment', checkAuthentication, function (req, res, next) {
  if(req.user && req.body.commentcontent){
    var commentData = new Comment({
      content: req.body.commentcontent,
      postId: req.body.commentpostid,
      userId: req.user._id
    });
    Comment.create(commentData, function (error, comment) {
      if (error) {
        console.log("add comment error!");
        res.redirect("https://projektweb20.herokuapp.com/posts?id=" + req.body.commentpostid);
      }
      else{
        res.redirect("https://projektweb20.herokuapp.com/posts?id=" + req.body.commentpostid);
      }
    }
  )
  }
});

//usunięcie komentarza
router.get('/removecomment/:postId/:commentId', checkAuthentication, function (req, res, next) {
  if(req.user.admin){
    Comment.remove({ _id: req.params.commentId }, function(err) {
      if (!err) {
        res.redirect("https://projektweb20.herokuapp.com/posts?id=" + req.params.postId);
      }
  });
  }
});


module.exports = router;
