var express = require('express');
var multer = require('multer');
var router = express.Router();
var Post = require('../models/post.js');
var Comment = require('../models/comment.js');
var User = require('../models/user.js');
var crypto = require("crypto");
var checkAuthentication = require('../utils/checkAuthentication');


//konkretny post
router.get('/', function(req, res) {
  Post.findOne({_id: req.query.id}, function(err, post) {
    if(!post){
      res.render("error", {message: "Oops! Something went wrong."});
    }
    else{
      Comment.aggregate([
        { "$match" : { postId : post._id }},
        {
        $lookup: {
            from: "users",
            localField: "userId",
            foreignField: "_id",
            as: "user"
        }
    }]).exec(function(err, comment) {
      res.render("onepost", { post: post, user: req.user, comments: comment});
    });
    }
  })
});

//usuwa post
router.get('/removepost/:postId', checkAuthentication, function (req, res, next) {
  if(req.user.admin){
    Post.remove({ _id: req.params.postId }, function(err) {
      Comment.remove({ postId: req.params.postId }, function(err) {
        if (!err) {
          res.redirect("/");
        }
    });
  });
  }else{
    res.render("error", {message: "Oops! Something went wrong."});
  }
});

//dodawanie postu form
router.get('/addpost', checkAuthentication, function(req, res, next) {
  res.render("addPost", { title: "New post", user: req.user });
});


var storage = multer.diskStorage({
destination: function (req, file, cb) {
  cb(null, './public/images/uploads')
},
filename: function (req, file, cb) {
  var fileName = crypto.randomBytes(20).toString('hex');
  let ext = file.originalname.substring(file.originalname.lastIndexOf('.'), file.originalname.length);
  cb(null, fileName + ext);
}
})

var upload = multer({ storage: storage })

//dodawanie postu
router.post('/addpost', checkAuthentication, upload.single('postpicture'), function (req, res, next) {

  if(req.user){
    if (req.body.posttitle &&
      req.body.postcontent ) {
  
      var postData = new Post({
        userId: req.user._id,
        title: req.body.posttitle,
        picture: req.file.filename,
        content: req.body.postcontent,
      });
  
      Post.create(postData, function (error, post) {
        if (error) {
          res.render("addPost", { title: "New post", user: req.user, message: "Oops! Something went wrong.", success: false });
        } else {
          res.render("addPost", { title: "New post", user: req.user, message: "Post added successfully.", success: true });
        }
      });
    } 
    else{
      res.render("addPost", { title: "New post", user: req.user, message: "Oops! Something went wrong.", success: false });
    }
  }
  else{
    res.render("error", {message: "Oops! Something went wrong."});
  }
});

//polubienie postu
router.post('/postlike', checkAuthentication, function (req, res) {
  if(req.user){
    var postid = req.body.postid;
    console.log(postid);
    Post.findOneAndUpdate({_id: postid}, {$inc:{likes: 1 }}, {new: false}, function(err, doc){
        if(err){
          res.render("error", {message: "Oops! Something went wrong."});
        }
    });
  }
  else{
    res.render("error", {message: "Oops! Something went wrong."});
  }
});


module.exports = router;
