var express = require('express');
var multer = require('multer');
var router = express.Router();
var Post = require('../models/post.js');
var checkAuthentication = require('../utils/checkAuthentication');

var message = "No posts to display."
/* GET home page. */
router.get('/', function(req, res, next) {
  Post.find({likes: {$gt: 50}}, function(err, allPosts) {
    if(allPosts.length > 0)
      res.render("index", { posts: allPosts, user: req.user });
    else
      res.render("emptyPosts", { message: message, user: req.user });  
    }).sort({date_send: -1});
});

router.get('/fresh', function(req, res, next) {
  Post.find({likes: {$lt: 51}}, function(err, allPosts) {
    if(allPosts.length > 0)
      res.render("index", { posts: allPosts, user: req.user });
    else
      res.render("emptyPosts", { message: message, user: req.user });  
  }).sort({date_send: -1});
});

router.get('/popular', function(req, res, next) {
  Post.find({likes: {$gt: 500}}, function(err, allPosts) {
      if(allPosts.length > 0)
        res.render("index", { posts: allPosts, user: req.user });
      else
        res.render("emptyPosts", { message: message, user: req.user });  
  }).sort({likes: -1});
});

router.get('/myposts', checkAuthentication, function(req, res, next) {

    Post.find({userId: req.user._id}, function(err, allPosts) {
      if(allPosts.length > 0)
        res.render("index", { posts: allPosts, user: req.user });
      else
        res.render("emptyPosts", { message: message, user: req.user });  
  }).sort({date_send: -1});
});



module.exports = router;
