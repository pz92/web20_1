var express = require('express');
var multer = require('multer');
var passport = require('passport');
var sha1 = require('sha1');
var router = express.Router();
var Post = require('../models/post.js');
var Comment = require('../models/comment.js');
var User = require('../models/user.js');
var crypto = require("crypto");
var mailerTransporter = require('../utils/mailerTransporter.js');
var emailStyle = require("../public/javascripts/email");

//zwraca wszystkich uzytkowników
router.get('/users', function(req, res) {
  User.find().lean().exec(function (err, users) {
    var message = "No users to display"
    if (!err) {
      if(users.length > 0){
        return res.end(JSON.stringify(users));
      }
      else{
        return res.end(JSON.stringify({message: message, status: false}));
      }
    }else{
      return res.end(JSON.stringify({message: message, status: false}));
    }
  });
});

//zwraca konkretnego uzytkownika
router.get('/user/:id', function(req, res) {
  User.findOne({ _id: req.params.id }).lean().exec(function (err, user) {
    var message = "No user to display"
    if (!err) {
      if(user){
        return res.end(JSON.stringify(user));
      }
      else{
        return res.end(JSON.stringify({message: message, status: false}));
      }
    }else{
      return res.end(JSON.stringify({message: message, status: false}));
    }  
  });
});

//zwraca wszystkie posty
router.get('/posts/all', function(req, res) {
  Post.find().lean().exec(function (err, posts) {
    var message = "No posts to display"
    if (!err) {
      if(posts.length > 0){
        return res.end(JSON.stringify(posts));
      }
      else{
        return res.end(JSON.stringify({message: message, status: false}));
      }
    }else{
      return res.end(JSON.stringify({message: message, status: false}));
    }
  });
});

//zwraca nowe posty
router.get('/posts/fresh', function(req, res) {
  Post.find({likes: {$lt: 51}}).lean().exec(function (err, posts) {
    if (!err) {
      if(posts.length > 0){
        return res.end(JSON.stringify(posts));
      }
      else{
        return res.end(JSON.stringify({message: message, status: false}));
      }
    }else{
      var message = "No posts to display"
      return res.end(JSON.stringify({message: message, status: false}));
    }
  });
});

//zwraca popularne posty
router.get('/posts/popular', function(req, res) {
  Post.find({likes: {$gt: 500}}).lean().exec(function (err, posts) {
    var message = "No posts to display"
    if (!err) {
      if(posts.length > 0){
        return res.end(JSON.stringify(posts));
      }
      else{
        return res.end(JSON.stringify({message: message, status: false}));
      }
    }else{
      return res.end(JSON.stringify({message: message, status: false}));
    }
  });
});

//zwaraca dany post
router.get('/post/:id', function(req, res) {
  Post.findOne({_id: req.params.id}).lean().exec(function (err, post) {
    var message = "No post to display"
    if (!err) {
      if(post){
        return res.end(JSON.stringify(post));
      }
      else{
        return res.end(JSON.stringify({message: message, status: false}));
      }
    }else{
      return res.end(JSON.stringify({message: message, status: false}));
    }
  });
});

//zwraca posty zalogowanego uzytkownika
router.get('/myposts', function(req, res, next) {
  if(req.user){
    var message = "No post to display"
    Post.find({userId: req.user._id}).lean().exec(function (err, posts) {
      if (!err) {
        if(posts.length>0){
          return res.end(JSON.stringify(posts));
        }
        else{
          return res.end(JSON.stringify({message: message, status: false}));
        }
      }else{
        return res.end(JSON.stringify({message: message, status: false}));
      }  
  });
  }
  else{
    var message = "You are not logged in"
    return res.end(JSON.stringify({message: message, status: false}));
  }
});

//zwraca tablicę komentarzy dla danego postu
router.get('/postcomments/:id', function(req, res) {
  Comment.find({ postId: req.params.id }).lean().exec(function (err, comments) {
    var message = "No comments to display"
    if (!err) {
      if(comments.length > 0){
        return res.end(JSON.stringify(comments));
      }
      else{
        return res.end(JSON.stringify({message: message, status: false}));
      }
    }else{
      return res.end(JSON.stringify({message: message, status: false}));
    }
  });
});

//zwraca konkretny komentarz
router.get('/comment/:id', function(req, res) {
  Comment.findOne({_id: req.params.id}).lean().exec(function (err, comment) {
    var message = "No comment to display"
    if (!err) {
      if(comment){
        return res.end(JSON.stringify(comment));
      }
      else{
        return res.end(JSON.stringify({message: message, status: false}));
      }
    }else{
      return res.end(JSON.stringify({message: message, status: false}));
    }
  });
});

//usuwanie postu
router.get('/removepost/:postId', function (req, res, next) {
  if(req.user){
    if(req.user.admin){
      Post.remove({ _id: req.params.postId }, function(err) {
        Comment.remove({ postId: req.params.postId }, function(err) {
          if (!err) {
            var message = "Post deleted."
            return res.end(JSON.stringify({ message: message, status: true}));
          }
          else{
            var message = "No post with this :id"
            return res.end(JSON.stringify({message: message, status: false}));
          }
      });
    });
    }
    else{
      var message = "You are not admin."
      return res.end(JSON.stringify({ message: message, status: false}));
    }
  }
  else{
    var message = "You are not logged in"
    return res.end(JSON.stringify({message: message, status: false}));
  }
});

//usuwanie komentarza
router.get('/removecomment/:commentId', function (req, res, next) {
  if(req.user){
    if(req.user.admin){
      Comment.remove({ _id: req.params.commentId }, function(err) {
        if (!err) {
          var message = "Comment deleted."
          return res.end(JSON.stringify({ message: message, status: true}));
        }
        else{
          var message = "No comment with this :id"
          return res.end(JSON.stringify({message: message, status: false}));
        }
    });
    }
    else{
      var message = "You are not admin."
      return res.end(JSON.stringify({ message: message, status: false}));
    }
  }
  else{
    var message = "You are not logged in"
    return res.end(JSON.stringify({message: message, status: false}));
  }
});

//logowanie uzytkownika
router.post('/login', function(req, res, next) {
  console.log(req.body.username + "  " + req.body.password)
  passport.authenticate('local', { session: true }, function(err, user, info) {
    User.findOne({username: req.body.username, active: false}, function(err, checkuser) {
      if (!user) { 
        if(checkuser){
            message = 'The account has not been activated.';
            return res.end(JSON.stringify({ username: req.body.username, message: message, status: false}));
          }
        else{
            message = 'Incorrect login or password';
            return res.end(JSON.stringify({ username: req.body.username, message: message, status: false}));
        }
      }
      req.logIn(user, function(err) {
        if (err) { return next(err); }
        message = 'Logged';
        return res.end(JSON.stringify({ username: req.body.username, message: message, status: true}));
      });
    })
  })(req, res, next);
});

//wylogowanie uzytkownika
router.get('/logout', function(req, res){
  req.logout();
  message = 'Logged Out';
  return res.end(JSON.stringify({ message: message, status: true}));
});

//rejestracja uzytkownika
router.post('/registration', function (req, res, next) {
  // confirm that user typed same password twice
  if (req.body.password !== req.body.confirmPassword) {
    message = 'Passwords do not match.';
    return res.end(JSON.stringify({ login: req.body.username, email: req.body.email, message: message, status: false}));
    return next(err);
  }

  if (req.body.username &&
    req.body.email &&
    req.body.password &&
    req.body.confirmPassword) {

    let token = crypto.randomBytes(50).toString('hex');

    var userData = new User({
      username: req.body.username,
      email: req.body.email,
      password: sha1(req.body.password),
      activateToken: token
    });

    User.create(userData, function (error, user) {
      if (error) {
        var message = "error";
        if (error.message.indexOf("username") !=-1) {
          message = "User with that login already exists.";
        }
        else if (error.message.indexOf("email") !=-1) {
          message = "User with that email already exists.";
        }
        return res.end(JSON.stringify({ login: req.body.username, email: req.body.email, message: message, status: false}));
      } else {
        var host = "https://projektweb20.herokuapp.com";
        var activationLink = host + '/users/activate?id=' + token;

        req.session.userId = user._id;
        let mailOptions = {
          from: '"mypage" <web20@t.pl>',
          to: req.body.email,
          subject: 'Confirm your mypage account, ' + req.body.username,
          html: `
          <div id="email">
            <h2>Hello ` + user.username + `</h2>
            <p>Confirm your email address to complete your mypage account, <span style="color: #17a2b8; font-weight: bold; font-size: 17px">` + user.username + `</span>.</p>
            
            <p>It's easy — just click the button below.</p>
            
            <p><a href="` + activationLink + `"><button class="btn btn-info">Confirm your account</button></a></p>
            
            <p>Or copy this link to your web browser:</p>
            <p>` + activationLink + `</p>
          </div>
          <style>
          ` + emailStyle + `
          </style>
          `
        };
        // wysyła maila dla ustawionej warstwy transportowej dla danych opcji 
        mailerTransporter.tTransporter.sendMail(mailOptions, (error, info) => {
        if (error) {
          return console.log(error);
        }
        });
        message = "We have sent a verification mail to " + req.body.email + ". Please activate your account with the link in this mail.";
        return res.end(JSON.stringify({ login: req.body.username, email: req.body.email, message: message, status: true}));
      }
    });
  } 
});

module.exports = router;
