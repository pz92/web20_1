var express = require('express');
var session = require('express-session');
var passport = require('passport');
var sha1 = require('sha1');
var crypto = require("crypto");

var router = express.Router();

var User = require("../models/user.js")
var mailerTransporter = require('../utils/mailerTransporter.js');
var emailStyle = require("../public/javascripts/email");

// wyświetlanie formularza do logowania
router.get('/login', function(req, res) {
  res.render("login", { title: "Log in"});
});

// wyświetlanie formularza do resetowania hasła
router.get('/resetpassword', function(req, res) {
  res.render("resetpass", { title: "Reset password"});
});

// wyświetlanie formularza do resetowania hasła
router.get('/newpassword', function(req, res, next) {
  if(req.query.id != null){
    User.findOne({resetToken: req.query.id}, function(err, checkuser) {
      if(checkuser){
        var currentDate = new Date();
        currentDate.setTime(currentDate.getTime() - 10*60*1000);  //current date - 10 mins
        if(checkuser.resetTokenDate > currentDate){
          return res.render("newpass", { title: "New password", resetToken: req.query.id });
        }
        else
        {
          var message = "Password reset link is expired"
          return res.render("resetpass", { title: "Reset password", message: message, success: false});
        }
      }  
      else{
        res.render("error", {message: "Oops! Something went wrong."});
      }
    });
  }
  else{
    res.render("error", {message: "Oops! Something went wrong."});
  }
});

router.post('/newpassword', function (req, res, next) {
  // confirm that user typed same password twice
  if (req.body.password !== req.body.confirmPassword) {
    var err = new Error('Passwords do not match.');
    err.status = 400;
    res.render("newpass", { title: "New password", message: "Passwords do not match." });
    return next(err);
  }

  if (req.body.password &&
    req.body.confirmPassword) {
      User.findOneAndUpdate( {resetToken: req.body.userid}, { $set:{password: sha1(req.body.password), resetToken: null, resetTokenDate: null} },
        {new: false}, function(err, data) {
          res.render("login", { title: "Log in", message: "Your password has been changed", success: true});
        });
  } 
});

// aktywowanie użytkownika o podanym w parametrze ID
router.get('/activate', function(req, res, next) {
  if(req.query.id != null){
    User.findOne({activateToken: req.query.id}, function(err, checkuser) {
      if(checkuser){
        User.findByIdAndUpdate( checkuser._id, { $set:{active:true, activateToken: null} },
          {new: false}, function(err, data) {
            return res.render("login", {message: "Your account is active, now you can Log In.", success: true});
          });
      }
        else
        {
          return res.render("error", {message: "Oops! Something went wrong."});
        }
      } )} 
      else{
        return res.render("error", {message: "Oops! Something went wrong."});
      }
});


router.post('/resetpassword', function (req, res, next) {

  if (req.body.email) {
    User.findOne({email: req.body.email}, function(err, checkuser) {
      if(!checkuser){
        message = "User with that email does not exist.";
        return res.render("resetpass", { title: "Reset password", message: message, success: false});
      }
      else{
        let token = crypto.randomBytes(50).toString('hex');

        var message = 'Account recovery email sent to ' + req.body.email;

        let userId = checkuser._id;
        var host = "https://projektweb20.herokuapp.com";
        var resetLink = host + '/users/newpassword?id=' + token;
      
        let mailOptions = {
          from: '"mypage" <web20@t.pl>',
          to: req.body.email,
          subject: 'Reset your password, ' + checkuser.username,
          html: `
          <div id="email">
            <h2>Hello ` + checkuser.username + `</h2>
            <p>We received a request to reset the password for your account, <span style="color: #17a2b8; font-weight: bold; font-size: 17px">` + checkuser.username + `</span>.</p>
            
            <p>If you made this request, click the button below. If you didn't make this request, you can ignore this email.</p>
            
            <p><a href="` + resetLink + `"><button class="btn btn-info">Reset your password</button></a></p>
            
            <p>Or copy this link to your web browser and reset your password:</p>
            <p>` + resetLink + `</p>
          </div>
          <style>
          ` + emailStyle + `
          </style>
          `

        };

        User.findOneAndUpdate({_id: userId}, {$set:{resetToken: token, resetTokenDate: Date.now() }}, {new: false},
            function(err, data) {
          // wysyła maila dla ustawionej warstwy transportowej dla danych opcji 
          mailerTransporter.tTransporter.sendMail(mailOptions, (error, info) => {
          if (error) {
            return console.log(error);
          }
          });
        });
        return res.render("resetpass", { title: "Reset password", message: message, success: true});
      }


    })

  } 
});



router.post('/login', function(req, res, next) {
  passport.authenticate('local', { session: true }, function(err, user, info) {
    User.findOne({username: req.body.username, active: false}, function(err, checkuser) {
      if (!user) { 
        var message;
        if(checkuser){
            message = 'The account has not been activated.';
            return res.render("login", { title: "Log In", message: message, username: req.body.username, activateinfo: true}); 
        }
        else{
            message = 'Incorrect login or password';
            return res.render("login", { title: "Log In", message: message}); 
        }
      }
      req.logIn(user, function(err) {
        if (err) { return next(err); }
        return res.redirect('/');
      });
    })
  })(req, res, next);
});

// wylogowanie i przekierowanie na stronę główną
router.get('/logout', function(req, res){
    req.logout();
    res.redirect('/');
});

router.get('/registration', function(req, res, next) {
  res.render("registration", { title: "Registration"});
});

router.post('/registration', function (req, res, next) {
  // confirm that user typed same password twice
  if (req.body.password !== req.body.confirmPassword) {
    var err = new Error('Passwords do not match.');
    err.status = 400;
    res.render("registration", { title: "Registration", login: req.body.username, email: req.body.email, message: "Passwords do not match." });
    return next(err);
  }

  if (req.body.username &&
    req.body.email &&
    req.body.password &&
    req.body.confirmPassword) {

    let token = crypto.randomBytes(50).toString('hex');

    var userData = new User({
      username: req.body.username,
      email: req.body.email,
      password: sha1(req.body.password),
      activateToken: token
    });

    User.create(userData, function (error, user) {
      if (error) {
        if (error.message.indexOf("username") !=-1) {
          message = "User with that login already exists.";
        }
        else if (error.message.indexOf("email") !=-1) {
          message = "User with that email already exists.";
        }
        res.render("registration", { title: "Registration", login: req.body.username, email: req.body.email, message: message });
      } else {
        var host = "https://projektweb20.herokuapp.com";
        var activationLink = host + '/users/activate?id=' + token;

        req.session.userId = user._id;
        let mailOptions = {
          from: '"mypage" <web20@t.pl>',
          to: req.body.email,
          subject: 'Confirm your mypage account, ' + req.body.username,
          html: `
          <div id="email">
            <h2>Hello ` + user.username + `</h2>
            <p>Confirm your email address to complete your mypage account, <span style="color: #17a2b8; font-weight: bold; font-size: 17px">` + user.username + `</span>.</p>
            
            <p>It's easy — just click the button below.</p>
            
            <p><a href="` + activationLink + `"><button class="btn btn-info">Confirm your account</button></a></p>
            
            <p>Or copy this link to your web browser:</p>
            <p>` + activationLink + `</p>
          </div>
          <style>
          ` + emailStyle + `
          </style>
          `
        };
        // wysyła maila dla ustawionej warstwy transportowej dla danych opcji 
        mailerTransporter.tTransporter.sendMail(mailOptions, (error, info) => {
        if (error) {
          return console.log(error);
        }
        });
        message = "We have sent a verification mail to " + req.body.email + ". Please activate your account with the link in this mail.";
        return res.render("registration", { title: "Registration", message: message, success: true });
      }
    });

  } 
});

router.get('/sendactivate', function (req, res, next) {
  // confirm that user typed same password twice
    User.findOne({username: req.query.user}, function(err, checkuser){
      if(checkuser){
        let token = crypto.randomBytes(50).toString('hex');

        User.findByIdAndUpdate( checkuser._id, { $set:{activateToken: token} },
        {new: false}, function(err, data) {
        var message = "We have sent a verification mail to " + checkuser.email + ". Please activate your account with the link in this mail.";

        var host = "https://projektweb20.herokuapp.com";
        var activationLink = host + '/users/activate?id=' + token;

        let mailOptions = {
          from: '"mypage" <web20@t.pl>',
          to: checkuser.email,
          subject: 'Confirm your mypage account, ' + checkuser.username,
          html: `
          <div id="email">
            <h2>Hello ` + checkuser.username + `</h2>
            <p>Confirm your email address to complete your mypage account, <span style="color: #17a2b8; font-weight: bold; font-size: 17px">` + checkuser.username + `</span>.</p>
            
            <p>It's easy — just click the button below.</p>
            
            <p><a href="` + activationLink + `"><button class="btn btn-info">Confirm your account</button></a></p>
            
            <p>Or copy this link to your web browser:</p>
            <p>` + activationLink + `</p>
          </div>
          <style>
          ` + emailStyle + `
          </style>
          `
        };
        // wysyła maila dla ustawionej warstwy transportowej dla danych opcji 
        mailerTransporter.tTransporter.sendMail(mailOptions, (error, info) => {
          if (error) {
            return console.log(error);
          }
          });
          return res.render("login", { title: "Log In", message: message, success: true});
          });
        
      }
      else{
        res.render("error", {message: "Oops! Something went wrong."});
      }
    })
  
});




module.exports = router;