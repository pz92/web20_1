var mongoose = require('mongoose');

var PostSchema= new mongoose.Schema({
    userId : mongoose.Schema.ObjectId,
    title : String, 
    content : String, 
    picture : String,
    date_send: { type: Date, default: Date.now },
    likes : { type: Number, default: 0 }
  }, {
    versionKey: false
  });

  //pierwszy argument to nazwa kolekcji w bazie (jak dam w liczbie pojedynczej to automatycznie jest to konwertowane na liczbe mnogą, wielkość liter niewazna), drugi do nazwa schematu
  var Post = mongoose.model('posts', PostSchema);

  module.exports = Post;
