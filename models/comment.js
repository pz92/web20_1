var mongoose = require('mongoose');

var commentSchema = new mongoose.Schema({
    content: { type: String, required: true},
    postId:  { type: mongoose.Schema.ObjectId, required: true},
    userId:  { type: mongoose.Schema.ObjectId, required: true},
	date_send: { type: Date, default: Date.now }
}, {
    versionKey: false
  });

module.exports = mongoose.model('comments', commentSchema);
