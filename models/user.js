var mongoose = require('mongoose');
var sha1 = require('sha1');

var userSchema = new mongoose.Schema({
    username: { type: String, required: true, unique: true},
    email: { type: String, required: true, unique: true},
	password: { type: String, required: true},
    admin: {type: Boolean, default: false},
    active: {type: Boolean, default: false},
    resetToken: String,
    resetTokenDate: Date,
    activateToken: String
}, {
    versionKey: false
  });

  // walidacja poprawności hasła
userSchema.methods.validPassword = function(pass) {
    return sha1(pass)==this.password;
};

userSchema.methods.accountActivate = function() {
  return this.active; 
};

module.exports = mongoose.model('users', userSchema);
